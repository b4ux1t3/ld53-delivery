using Godot;
using System;

public partial class Pivot : Node2D
{
	[Export] public float AngularVelocity = Mathf.Pi * 0.05f;
	[Export] public bool ShouldRotate;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (ShouldRotate)
		{
			Rotate(AngularVelocity * (float) delta);
		}	
	}
}
