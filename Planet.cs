using Godot;
using System;
using System.Linq;
using Godot.Collections;

public partial class Planet : Area2D
{
	[Export] public float RotationSpeed  = -Mathf.Pi * 0.1f;
	[Export] public bool ShouldRotate = false;
	[Export] public Node2D TargetParent;

	[Export] public Texture2D Sprite;

	[Export] public string PlanetName;

	[Export] public float GravityRadius;
	[Export] public float GravityStrength;

	private Sprite2D _myImage;
	public override void _Ready()
	{
		_myImage = GetNode<Sprite2D>("Image");
		_myImage.Texture = Sprite;

		((CircleShape2D) GetNode<CollisionShape2D>("Orbit").Shape).Radius = GravityRadius;
		Gravity = GravityStrength;
		
		QueueRedraw();
	}

	public override void _PhysicsProcess(double delta)
	{
		if (ShouldRotate && Mathf.Abs(RotationSpeed) > 0)
		{
			Rotate(RotationSpeed * (float) delta);
		}
	}

	private void _on_solar_system_spawn_target(string spawnTarget, float timer)
	{
		if (spawnTarget != PlanetName) return;
		var spawnables = TargetParent.GetChildren().Where(c => c is Target {Spawned: false}).ToArray();
		var choice = (Target) spawnables[Random.Shared.Next(spawnables.Length)];
		choice?.Spawn(timer);
	}

	private void _on_system_view_toggled_pushed(bool state)
	{
		_myImage.Scale *= state ? 10 : 0.1f;
	}
}
