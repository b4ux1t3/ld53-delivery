using Godot;
using System;

public partial class SolarSystem : Node2D
{

	[Signal]
	public delegate void SpawnTargetEventHandler(string planetName, float timer);
	
	[Signal]
	public delegate void SystemViewToggledEventHandler(bool inSystemView);

	[Signal]
	public delegate void SystemViewUnlockedEventHandler();
	
	private Camera2D _cam;
	private RemoteTransform2D currentRemote;

	[Export] public RemoteTransform2D EarthRemote;
	[Export] public RemoteTransform2D SunRemote;

	[Export] public float MinimumZoom = 0.1f;
	[Export] public float MaximumZoom = 1;

	[Export] public float ZoomFactor = 0.2f;

	[Export] public string[] PlanetNames;
	[Export] public float SpawnTime = 15;
	[Export] public float SystemWideSpawnTime = 30;

	[Export] public int InterplanetaryContractThreshold;
	private ScoreManager _scoreManager;
	

	private Timer _timer;

	public bool InSystemView = false;
	private bool _interplanetaryDeliveriesEnabled;

	public override void _Ready()
	{
		_scoreManager = GetNode<ScoreManager>("ScoreManager");
		
		_cam = GetNode<Camera2D>("Camera2D");

		currentRemote = EarthRemote;

		_timer = new Timer();
		_timer.Autostart = true;
		AddChild(_timer);
		_timer.Connect("timeout", Callable.From(_spawnPlanetCallback));
		_timer.Start(SpawnTime);
		
		_spawnPlanetCallback();
	}	

	private void _on_gui_zoom_in_button_pressed()
	{
		if (InSystemView || _cam.Zoom.X * (1 + ZoomFactor) > MaximumZoom) return;

		_cam.Zoom *= 1 + ZoomFactor;
	}
	
	private void _on_gui_zoom_out_button_pressed()
	{
		if (InSystemView || _cam.Zoom.X * (1 - ZoomFactor) < MinimumZoom) return;
		_cam.Zoom *= 1 - ZoomFactor;
	}

	private void _on_gui_system_view_button_pressed(bool state)
	{
		currentRemote.RemotePath = "";
		InSystemView = state;
		currentRemote = state ? SunRemote : EarthRemote;
		_cam.Zoom = state ? Vector2.One * ZoomFactor : Vector2.One * 0.5f;
		currentRemote.RemotePath = _cam.GetPath();

		EmitSignal(SignalName.SystemViewToggled, state);
	}

	private void _on_score_updated(int newScore)
	{
		if (_interplanetaryDeliveriesEnabled || newScore < InterplanetaryContractThreshold) return;
		_interplanetaryDeliveriesEnabled = true;

		EmitSignal(SignalName.SystemViewUnlocked);
		
		// Removes Earth.
		PlanetNames = PlanetNames[1..];
	}

	private void _spawnPlanetCallback()
	{
		if (!_interplanetaryDeliveriesEnabled)
		{
			EmitSignal(SignalName.SpawnTarget, "Earth", SpawnTime);
			_timer.Start(SpawnTime);
			return;
		}

		var name = PlanetNames[Random.Shared.Next(PlanetNames.Length)];
		EmitSignal(SignalName.SpawnTarget, name, SystemWideSpawnTime);
		_timer.Start(SpawnTime);
	}

}
