using Godot;
using System;

public partial class Projectile : RigidBody2D
{
	private SolarSystem _parent;

	private Sprite2D _sprite;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_parent = GetParent<SolarSystem>();
		_parent.Connect(SolarSystem.SignalName.SystemViewToggled, Callable.From<bool, bool>(_toggleSpriteSize));
		
		_sprite = GetNode<Sprite2D>("Sprite2D");
		
		_sprite.Scale = _parent.InSystemView ? Vector2.One * 2 : Vector2.One * 0.2f;

		GetTree().CreateTimer(60).Connect(SceneTreeTimer.SignalName.Timeout, Callable.From(QueueFree));
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	private bool _toggleSpriteSize(bool state)
	{
		_sprite.Scale *= _parent.InSystemView ? 10 : 0.1f;
		return state;
	}
}
