using Godot;
using System;

public partial class LaunchLocation : Node2D
{
	
	[Export] public float MaxAngle = 0.5f * Mathf.Tau - 0.05f * Mathf.Tau;
	[Export] public float MinAngle = 0.05f * Mathf.Tau;

	private bool _inSystemView = false;
	
	private float _takeOffAngle;

	[Export] public float MaxPower = 5;
	[Export] public float MinPower = 1;
	[Export] public float MaxSystemViewPower;
	[Export] public float MinSystemViewPower;
	private float _launchPower;

	private Node2D _launchPivot;
	private RemoteTransform2D _remTrans;

	private PackedScene _projectileScene;

	[Signal]
	public delegate void LaunchAngleTextChangedEventHandler(string launchAngleText);
	[Signal]
	public delegate void LaunchPowerTextChangedEventHandler(string powerText);
	
	[Export] public Node2D Earth;

	private bool _canShoot;
	private double _shootTimer;
	[Export] public double ShootTimerSetting = 0.3;

	public override void _Ready()
	{
		_launchPivot = GetNode<Node2D>("LaunchPivot");

		_launchPivot.Rotation = MinAngle;

		_projectileScene = (PackedScene) GD.Load("res://Scenes/projectile.tscn");

		_launchPower = MinPower;

		
		
		EmitSignal(SignalName.LaunchAngleTextChanged, _takeOffAngle);
		EmitSignal(SignalName.LaunchPowerTextChanged, _launchPower);
		
	}

	public override void _Process(double delta)
	{
		_shootTimer -= delta;

		if (_shootTimer < 0)
		{
			_canShoot = true;
		}
	}

	private void _rotateLaunchPivot()
	{
		_launchPivot.Rotation = _takeOffAngle;
	}

	private void _launchProjectile()
	{
		if (!_canShoot) return;
		var newProj = _projectileScene.Instantiate<Projectile>();
		
		GetTree().CurrentScene.AddChild(newProj);
		newProj.GlobalPosition = GlobalPosition;
		// newProj.GlobalRotation = _launchPivot.GlobalRotation; 
		// var launchVector = Vector2.Down.Rotated(_takeOffAngle) * _launchPower;
		// var launchVector = Vector2.Down.Rotated(_takeOffAngle + GlobalRotation) * _launchPower;
		var launchVector = Vector2.Down.Rotated(_launchPivot.GlobalRotation) * _launchPower;
		newProj.ApplyCentralImpulse(launchVector);
		// newProj.ApplyCentralImpulse(Vector2.Down * _launchPower);

		_shootTimer = ShootTimerSetting;
		_canShoot = false;
	}

	private void _on_moon_influence_launch_angle_changed(float newAngle) {
		_takeOffAngle = Mathf.Remap(newAngle, 0, 1024, MinAngle, MaxAngle);
		_rotateLaunchPivot();
		EmitSignal(SignalName.LaunchAngleTextChanged, $"{Mathf.Tau * 0.25  - _takeOffAngle:F2}");
	}


	private void _on_moon_influence_launch_power_changed(float newPower)
	{
		_launchPower = _inSystemView 
			? Mathf.Remap(newPower, 0, 1024, MinSystemViewPower, MaxSystemViewPower) 
			: Mathf.Remap(newPower, 0, 1024, MinPower, MaxPower);

		EmitSignal(SignalName.LaunchPowerTextChanged, $"{_launchPower:F2}");
	}
	private void _on_moon_influence_launch_projectile()
	{
		_launchProjectile();
	}

	private void _on_system_view(bool state)
	{
		_inSystemView = state;
		var sprite = GetNode<Sprite2D>("LaunchPivot/Sprite2D");
		sprite.Scale *= state ? 10 : 0.1f;

		if (_inSystemView)
		{
			_launchPower = Mathf.Remap(_launchPower, MinPower, MaxPower, MinSystemViewPower, MaxSystemViewPower);
			sprite.Position = new Vector2(sprite.Position.X, sprite.Position.Y * 10);
		}
		else
		{
			_launchPower = Mathf.Remap(_launchPower, MinSystemViewPower, MaxSystemViewPower, MinPower, MaxPower);
			sprite.Position = new Vector2(sprite.Position.X, sprite.Position.Y / 10);
		}
		
		EmitSignal(SignalName.LaunchPowerTextChanged, $"{_launchPower:F2}");
		
	}
}
