using Godot;
using System;

public partial class Gui : CanvasLayer
{
	/* EXTERNAL SIGNALS */
	[Signal]
	public delegate void LaunchButtonPressedEventHandler();
	[Signal]
	public delegate void SystemViewButtonPressedEventHandler(bool state);
	[Signal]
	public delegate void AngleChangedEventHandler(float newAngle);
	[Signal]
	public delegate void PowerChangedEventHandler(float newPower);

	private Label _angleLabel;
	private Label _powerLabel;
	private Label _scoreLabel;

	private Label _timerLabel;

	private bool _timerGoing = false;

	private Timer _timer;

	public override void _Ready()
	{
		_angleLabel = GetNode<Label>("AngleSlider/AngleLabel");
		_powerLabel = GetNode<Label>("PowerSlider/PowerLabel");
		_scoreLabel = GetNode<Label>("ScoreLabel");
		_timerLabel = GetNode<Label>("TimerLabel");

		_timerLabel.Visible = false;
		
		_timer = new();
		_timer.Autostart = false;
		AddChild(_timer);

		_timer.Connect(Timer.SignalName.Timeout, Callable.From(_clearScreenTimer));
	}

	public override void _Process(double delta)
	{
		if (!_timerGoing) return;
		_timerLabel.Text = $"{_timer.TimeLeft:F2}";
	}

	private void _on_system_view_toggled(bool state) => EmitSignal(SignalName.SystemViewButtonPressed, state);
	private void _on_angle_slider_value_changed(float newAngle) => EmitSignal(SignalName.AngleChanged, newAngle);
	private void _on_power_slider_value_changed(float newPower) => EmitSignal(SignalName.PowerChanged, newPower);
	private void _on_launch_button_pressed() => EmitSignal(SignalName.LaunchButtonPressed);

	private void _on_moon_influence_launch_angle_text_updated(string newText) => _angleLabel.Text = newText;
	private void _on_moon_influence_launch_power_text_updated(string newText) => _powerLabel.Text = newText;
	private void _on_score_updated(int newScore) => _scoreLabel.Text = $"{newScore:N}";

	private void _on_solar_system_view_unlocked() => GetNode<CheckButton>("SystemView").Disabled = false;

	public void RegisterTarget(Target target)
	{

		target.Connect(Target.SignalName.OnSpawned, Callable.From<float, bool>(_setScreenTimer));
	}

	private bool _setScreenTimer(float timer)
	{
		_timerLabel.Visible = true;
		_timerLabel.Text = $"{timer:F2}";
		_timer.Start(timer - 0.1);
		_timerGoing = true;
		return true;
	}

	private void _clearScreenTimer()
	{
		_timerLabel.Visible = false;
		_timerGoing = false;
	}
	
}
