using Godot;
using System;

public partial class ScoreManager : Node2D
{
	[Signal]
	public delegate void ScoreChangedEventHandler(int newScore);

	[Export] public int DeliveryFailurePenalty = 10;
	[Export] public int HeliumDeliveryBasePoints = 10;

	[Export] public EarthMoon EarthMoonSystem;
	[Export] public float ScoreCoefficient = 0.1f;
	
	private int _score =0;

	public int Score
	{
		get => _score;
		private set
		{
			_score = value;
			EmitSignal(SignalName.ScoreChanged, _score);
		}
	}
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	private void _on_failed_delivery()
	{
		Score -= DeliveryFailurePenalty;
	}

	private bool _on_helium_received(Vector2 pos)
	{
		Score += HeliumDeliveryBasePoints * Mathf.FloorToInt(pos.DistanceTo(EarthMoonSystem.GlobalPosition) * ScoreCoefficient);
		return true;
	}

	private bool _on_delivery_completed(int points)
	{
		Score += points;
		return true;
	}

	public void RegisterNewTarget(Target newTarget)
	{
		newTarget.Connect(Target.SignalName.FailedDelivery, Callable.From(_on_failed_delivery));
		newTarget.Connect(Target.SignalName.HeliumReceived, Callable.From<Vector2, bool>(_on_helium_received));
		newTarget.Connect(Target.SignalName.DeliverySuccess, Callable.From<int, bool>(_on_delivery_completed));
	}
}
