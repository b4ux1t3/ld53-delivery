using Godot;
using System;

public partial class EarthMoon : Node2D
{
	/* EXTERNAL SIGNALS */
	[Signal]
	public delegate void AngleTextUpdatedEventHandler(string angleText);
	[Signal]
	public delegate void PowerTextUpdatedEventHandler(string powerText);
	
	/* INTERNAL SIGNALS */
	[Signal]
	public delegate void LaunchButtonPushedEventHandler();
	[Signal]
	public delegate void SystemViewToggledPushedEventHandler(bool state);
	[Signal]
	public delegate void AngleChangedEventHandler(float value);
	[Signal]
	public delegate void PowerChangedEventHandler(float value);
	[Signal]
	public delegate void OnSpawnPlanetEventHandler(string planetName);
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	private void _on_gui_launch_button_pressed() => EmitSignal(SignalName.LaunchButtonPushed);
	private void _on_gui_angle_changed(float newValue) => EmitSignal(SignalName.AngleChanged, newValue);
	private void _on_gui_power_changed(float newValue) => EmitSignal(SignalName.PowerChanged, newValue);
	
	private void _on_moon_influence_launch_angle_text_updated(string text) =>
		EmitSignal(SignalName.AngleTextUpdated, text);
	private void _on_moon_influence_launch_power_text_updated(string text) =>
		EmitSignal(SignalName.PowerTextUpdated, text);

	private void _on_solar_system_system_view_toggled(bool state) => EmitSignal(SignalName.SystemViewToggledPushed, state);

	private void _on_solar_system_spawn_target(string planetName, float timer) => EmitSignal(SignalName.OnSpawnPlanet, planetName, timer);
}

