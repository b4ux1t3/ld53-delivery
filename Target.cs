using Godot;
using System;
using System.Collections.Concurrent;

public partial class Target : Area2D
{
	[Export] public bool Spawned;
	[Export] public bool OverrideTimer;

	[Export] private CircleShape2D TargetGeometry;

	[Export] public float DeliveryTime = 30f;
	private bool _failed;

	[Signal]
	public delegate void DeliverySuccessEventHandler(int newPoints);
	
	[Signal]
	public delegate void HeliumReceivedEventHandler(Vector2 position);

	[Signal]
	public delegate void FailedDeliveryEventHandler();

	[Signal]
	public delegate void OnSpawnedEventHandler(float timer);
	public int Points => TargetGeometry.Radius switch
	{
		< 100 => 500,
		< 200 => 100,
		_ => 50
	};

	public int HeliumNeeded => TargetGeometry.Radius switch
	{
		< 100 => 5,
		< 200 => 10,
		_ => 20
	};
	
	private int _heliumReceived = 0;

	private Timer _timer;

	private Sprite2D _mySprite;

	private Gui _gui;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		var collider = GetNode<CollisionShape2D>("CollisionShape2D");
		collider.Shape ??= TargetGeometry;
		_timer = new();
		AddChild(_timer);
		_timer.Connect("timeout", Callable.From(_timerTimeout));

		_mySprite = GetNode<Sprite2D>("Sprite2D");
		_mySprite.Visible = false;
		
		GetNode<ScoreManager>("/root/SolarSystem/ScoreManager").RegisterNewTarget(this);

		_gui = GetNode<Gui>("/root/SolarSystem/GUI");
		_gui.RegisterTarget(this);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		// QueueRedraw();
	}

	// public override void _Draw()
	// {
	// 	if (!Spawned) return;
	// 	
	// 	DrawCircle(Position, TargetGeometry.Radius, Colors.Green);
	// }

	public void Spawn(float spawnTime)
	{
		DeliveryTime = spawnTime;
		_heliumReceived = 0;
		Spawned = true;
		_failed = true;
		_timer.Start(DeliveryTime);
		_mySprite.Visible = true;

		EmitSignal(SignalName.OnSpawned, DeliveryTime);

	}

	private void _despawn()
	{
		Spawned = false;
		if (_failed) EmitSignal(SignalName.FailedDelivery);
		_mySprite.Visible = false;
	}

	private void _timerTimeout()
	{
		if (OverrideTimer) return;
		_despawn();
	}

	private void _on_body_entered(Node2D body)
	{
		if (!Spawned || body is not Projectile) return;
		GD.Print($"Helium delivery entered ${Name}!");
		_heliumReceived++;
		GD.Print($"Helium: {_heliumReceived}/{HeliumNeeded}");
		body.QueueFree();
		if (_heliumReceived >= HeliumNeeded)
		{
			GD.Print("Successful helium delivery");
			_failed = false;
			_despawn();
			EmitSignal(SignalName.DeliverySuccess, Points);
		}
		else
		{
			EmitSignal(SignalName.HeliumReceived, GlobalPosition);
		}
	}
}
