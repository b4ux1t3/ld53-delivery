using Godot;
using System;

public partial class MoonSystem : Area2D
{
	/* EXTERNAL SIGNALS */
	[Signal]
	public delegate void LaunchAngleTextUpdatedEventHandler(string newAngle);
	[Signal]
	public delegate void LaunchPowerTextUpdatedEventHandler(string newPower);

	/* INTERNAL SIGNALS */
	[Signal]
	public delegate void LaunchAngleChangedEventHandler(float newAngle);
	
	[Signal]
	public delegate void LaunchPowerChangedEventHandler(float newPower);
	
	[Signal]
	public delegate void LaunchProjectileEventHandler();

	[Signal]
	public delegate void SystemViewEventHandler(bool state);
	
	
	private void _on_launch_angle_changed(float newAngle) {

		EmitSignal(SignalName.LaunchAngleChanged, newAngle);
	}
	
	private void _on_power_slider_value_changed(float newPower)
	{

		EmitSignal(SignalName.LaunchPowerChanged, newPower);
	}
	
	private void _on_launch_button_pressed()
	{
		EmitSignal(SignalName.LaunchProjectile);
	}

	private void _on_launch_location_launch_angle_text_changed(string text) =>
		EmitSignal(SignalName.LaunchAngleTextUpdated, text); 
	
	private void _on_launch_location_launch_power_text_changed (string text) =>
		EmitSignal(SignalName.LaunchPowerTextUpdated, text);


	private void _on_system_view_toggled_pushed(bool state) => 
		EmitSignal(SignalName.SystemView, state);
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
		
	}
	

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		
	}
}
